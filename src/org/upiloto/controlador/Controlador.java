package org.upiloto.controlador;

import java.io.IOException;
import java.text.ParseException;

import javax.swing.ComboBoxModel;

import org.upiloto.controlador.estructuras.ArbolPagos;
import org.upiloto.controlador.estructuras.ArbolProductos;
import org.upiloto.controlador.estructuras.ArbolProveedores;
import org.upiloto.controlador.estructuras.ArbolProveedores.Nodo;
import org.upiloto.controlador.excel.GeneradorExcel;
import org.upiloto.modelo.Pago;
import org.upiloto.modelo.Producto;
import org.upiloto.modelo.Proveedor;
/**
 * clase controlador
 */
public class Controlador {
	//singleton, crea una instancia unica para el objeto 
	private static Controlador mc=null;
	private ArbolPagos arbolPagos = new ArbolPagos();
	private ArbolProductos arbolProductos = new ArbolProductos();
	private ArbolProveedores arbolProveedor = new ArbolProveedores();
	
	public static Controlador getInstance(){
		if (mc==null){
			mc=new Controlador();
		}
		return mc;
	}
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//

	/**
	*M�todo para guardar los datos del proveedor
	*/ 
	public void guardarProveedor(Proveedor proveedor) {
		arbolProveedor.insertar(proveedor);
	}
	
/**
 *  busca a un proveedor por el c�digo
*/  
	public Proveedor obtenerProveedorPorCodigo(int codigo) {
		return arbolProveedor.obtenerPorCodigo(codigo);
	}
	
/**
*M�todo para eliminar el proveedor
*/
	public void eliminarProveedor(Proveedor proveedor) {
		arbolProveedor.eliminar(proveedor);
	}
	
/**
 *  Guarda la informaci�n del nuevo producto 
 */
	public void guardarProducto(Producto producto) {
		arbolProductos.insertar(producto);
	}
	
/**
 *Busca un producto por c�digo
	*/
	public Producto obtenerProductoPorCodigo(int codigo) {
		return arbolProductos.obtenerPorCodigo(codigo);
	}
	
	//M�todo para eliminar el producto
	public void eliminarProducto(Producto producto) {
		arbolProductos.eliminar(producto);
	}
	
	
/**
 * Guarda la informaci�n del pago realizado
 */
	public void guardarPago(Pago pago) {
		arbolPagos.insertar(pago);
	}
	
	/**
	 * M�todo para generar el listado de productos en excel 
	 */
	public void generarExcelProductos() {
		GeneradorExcel generadorExcel = new GeneradorExcel();
		try {
			generadorExcel.generarConProductos(arbolProductos);
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public ArbolPagos getArbolPagos() {
		return arbolPagos;
	}

	public void setArbolPagos(ArbolPagos arbolPagos) {
		this.arbolPagos = arbolPagos;
	}

	public ArbolProductos getArbolProductos() {
		return arbolProductos;
	}

	public void setArbolProductos(ArbolProductos arbolProductos) {
		this.arbolProductos = arbolProductos;
	}

	public ArbolProveedores getArbolProveedor() {
		return arbolProveedor;
	}

	public void setArbolProveedor(ArbolProveedores arbolProveedor) {
		this.arbolProveedor = arbolProveedor;
	}

	
	//El codigo de aqui para abajo sirve para generar los arreglos necesarios 
	//para usar en el combo box, el combo recibe un arreglo y no un arbol, asi que 
	//los siguientes metodos sirven para obtener los proveedores como arreglos de proveedores
	// y como arreglos de texto
	private int auxContadorProveedor = 0;
	private String[] proveedoresString;
	public String[] obtenerTodosProveedoresString() {
		auxContadorProveedor = 0;
		proveedoresString = new String[arbolProveedor.getTama�o()];
		agregarProveedorString(arbolProveedor.getRaiz());
		return proveedoresString;
	}

	private void agregarProveedorString(Nodo nodoProveedor) {
		if(nodoProveedor.left!=null){
			agregarProveedorString(nodoProveedor.left);
		}
		
		proveedoresString[auxContadorProveedor] = nodoProveedor.proveedor.getNombre();
		auxContadorProveedor++;
		
		if(nodoProveedor.right!=null){
			agregarProveedorString(nodoProveedor.right);
		}
	}
	
	private Proveedor[] proveedores;
	public Proveedor[] obtenerTodosProveedores() {
		auxContadorProveedor = 0;
		proveedores = new Proveedor[arbolProveedor.getTama�o()];
		if(arbolProveedor.getTama�o()>0)
			agregarProveedor(arbolProveedor.getRaiz());
		return proveedores;
	}

	private void agregarProveedor(Nodo nodoProveedor) {
		if(nodoProveedor.left!=null){
			agregarProveedor(nodoProveedor.left);
		}
		
		proveedores[auxContadorProveedor] = nodoProveedor.proveedor;
		auxContadorProveedor++;
		
		if(nodoProveedor.right!=null){
			agregarProveedor(nodoProveedor.right);
		}
	}

}
