package org.upiloto.controlador.estructuras;

import org.upiloto.modelo.Producto;

// Clase del �rbol binario para los productos  
public class ArbolProductos {
	
	private Nodo raiz = null;
	private int tama�o = 0;
	
	public class Nodo {
        public Nodo left;
        public Nodo right;
        public Producto producto;
        public Nodo(Producto producto) {
            this.producto = producto;
        }
    }
	//m�todo que llama la funci�n recursiva insertar sin nodo
	public void insertar(Producto producto){
		if(raiz == null){
			raiz = new Nodo(producto);
			tama�o=1;
		}
		insertar(raiz,producto);
	}
	//metodo interno de la clase para utilizarse recursivamente en el arbol
	public void insertar(Nodo nodo, Producto producto){
		if ( producto.getCodigo() < nodo.producto.getCodigo() ) {
            if (nodo.left != null) {
            	insertar(nodo.left, producto);
            } else {
                nodo.left = new Nodo(producto);
                tama�o++;
            }
        } else if ( producto.getCodigo() > nodo.producto.getCodigo() ) {
            if (nodo.right != null) {
            	insertar(nodo.right, producto);
            } else {
                nodo.right = new Nodo(producto);
                tama�o++;
            }
        }
	}
	//m�todo que llama la funci�n recursiva actualizar sin nodo
	public void actualizar(Producto producto){
		actualizar(raiz,producto);
	}
	//metodo interno de la clase para utilizarse recursivamente en el arbol
	private void actualizar(Nodo nodo, Producto producto){
		if ( producto.getCodigo() < nodo.producto.getCodigo() ) {
            if (nodo.left != null) {
            	actualizar(nodo.left, producto);
            }
        } else if ( producto.getCodigo() > nodo.producto.getCodigo() ) {
            if (nodo.right != null) {
            	actualizar(nodo.right, producto);
            }
        }else if(producto.getCodigo() == nodo.producto.getCodigo()){
        	nodo.producto = producto;
        }
	}
	//m�todo que llama la funci�n recursiva eliminar sin el nodo
		public void eliminar(Producto producto){
		eliminar(raiz,producto);
	}
	//metodo interno de la clase para utilizarse recursivamente en el arbol
	private void eliminar(Nodo nodo, Producto producto){
		if ( producto.getCodigo() < nodo.producto.getCodigo() ) {
            if (nodo.left != null) {
            	eliminar(nodo.left, producto);
            }
        } else if ( producto.getCodigo() > nodo.producto.getCodigo() ) {
            if (nodo.right != null) {
            	eliminar(nodo.right, producto);
            }
        }else if(producto.getCodigo() == nodo.producto.getCodigo()){
        	nodo.producto = null;
        	if(raiz.producto == producto){
        		raiz=null;
        	}
        }
	}
	//metodo para obtener el nodo de la raiz
	public Nodo getRaiz() {
		return raiz;
	}
	//m�todo que llama la funci�n recursiva obtenerPorCodigo sin nodo

	public Producto obtenerPorCodigo(int codigo) {
		return obtenerPorCodigo(raiz,codigo);
	}
	//metodo interno de la clase para utilizarse recursivamente en el arbol

	private Producto obtenerPorCodigo(Nodo raiz, int codigo) {
		if(raiz.producto.getCodigo() < codigo){
			return obtenerPorCodigo(raiz.left, codigo);
		}else if(raiz.producto.getCodigo() > codigo){
			return obtenerPorCodigo(raiz.right, codigo);
		}else if(raiz.producto.getCodigo() == codigo){
			return raiz.producto;
		}else{
			return null;
		}
	}
//metodo para obtener el numero de elemntos en el arbol
	public int getTama�o() {
		return tama�o;
	}
	
}
