package org.upiloto.controlador.estructuras;

import org.upiloto.modelo.Pago;

//Clase del �rbol binario para los pagos  

public class ArbolPagos {
	
	private Nodo raiz = null;
	
	public class Nodo {
        public Nodo left;
        public Nodo right;
        public Pago pago;
        public Nodo(Pago pago) {
            this.pago = pago;
        }
    }
	//m�todo que llama la funci�n recursiva insertar sin nodo

	public void insertar(Pago pago){
		if(raiz == null){
			raiz = new Nodo(pago);
		}
		insertar(raiz,pago);
	}
	//metodo interno de la clase para utilizarse recursivamente en el arbol

	private void insertar(Nodo nodo, Pago pago){
		if ( pago.getReferencia() < nodo.pago.getReferencia() ) {
            if (nodo.left != null) {
            	insertar(nodo.left, pago);
            } else {
                nodo.left = new Nodo(pago);
            }
        } else if ( pago.getReferencia() > nodo.pago.getReferencia() ) {
            if (nodo.right != null) {
            	insertar(nodo.right, pago);
            } else {
                nodo.right = new Nodo(pago);
            }
        }
	}
	//m�todo que llama la funci�n recursiva actualizar sin nodo
	public void actualizar(Pago pago){
		actualizar(raiz,pago);
	}
	//metodo interno de la clase para utilizarse recursivamente en el arbol

	private void actualizar(Nodo nodo, Pago pago){
		if ( pago.getReferencia() < nodo.pago.getReferencia() ) {
            if (nodo.left != null) {
            	actualizar(nodo.left, pago);
            }
        } else if ( pago.getReferencia() > nodo.pago.getReferencia() ) {
            if (nodo.right != null) {
            	actualizar(nodo.right, pago);
            }
        }else if(pago.getReferencia() == nodo.pago.getReferencia()){
        	nodo.pago = pago;
        }
	}
	//m�todo que llama la funci�n recursiva eliminar sin el nodo
	public void eliminar(Pago pago){
		eliminar(raiz,pago);
	}
	//metodo interno de la clase para utilizarse recursivamente en el arbol
	private void eliminar(Nodo nodo, Pago pago){
		if ( pago.getReferencia() < nodo.pago.getReferencia() ) {
            if (nodo.left != null) {
            	eliminar(nodo.left, pago);
            }
        } else if ( pago.getReferencia() > nodo.pago.getReferencia() ) {
            if (nodo.right != null) {
            	eliminar(nodo.right, pago);
            }
        }else if(pago.getReferencia() == nodo.pago.getReferencia()){
        	nodo.pago = null;
        	if(raiz.pago == pago){
        		raiz=null;
        	}
        }
	}
	//metodo para obtener el nodo de la raiz
	public Nodo getRaiz(){
		return raiz;
	}
	
}
