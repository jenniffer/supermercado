package org.upiloto.controlador.estructuras;

import org.upiloto.modelo.Proveedor;

//Clase del �rbol binario para los proveedores  
public class ArbolProveedores {
	
	private Nodo raiz = null;
	private int tama�o;
	//clase interna que define el nodo
	public class Nodo {
        public Nodo left;
        public Nodo right;
        public Proveedor proveedor;
        public Nodo(Proveedor proveedor) {
            this.proveedor = proveedor;
        }
    }
	//m�todo que llama la funci�n recursiva insertar sin nodo
	public void insertar(Proveedor proveedor){
		if(raiz == null){
			raiz = new Nodo(proveedor);
			tama�o=1;
		}
		insertar(raiz,proveedor);
	}
	//metodo interno de la clase para utilizarse recursivamente en el arbol
	public void insertar(Nodo nodo, Proveedor proveedor){
		if ( proveedor.getCodigo() < nodo.proveedor.getCodigo() ) {
            if (nodo.left != null) {
            	insertar(nodo.left, proveedor);
            } else {
                nodo.left = new Nodo(proveedor);
                tama�o++;
            }
        } else if ( proveedor.getCodigo() > nodo.proveedor.getCodigo() ) {
            if (nodo.right != null) {
            	insertar(nodo.right, proveedor);
            } else {
                nodo.right = new Nodo(proveedor);
                tama�o++;
            }
        }
	}
	//m�todo que llama la funci�n recursiva actualizar sin nodo
		public void actualizar(Proveedor proveedor){
		actualizar(raiz, proveedor);
	}
		//metodo interno de la clase para utilizarse recursivamente en el arbol
	private void actualizar(Nodo nodo, Proveedor proveedor){
		if ( proveedor.getCodigo() < nodo.proveedor.getCodigo() ) {
            if (nodo.left != null) {
            	actualizar(nodo.left, proveedor);
            }
        } else if ( proveedor.getCodigo() > nodo.proveedor.getCodigo() ) {
            if (nodo.right != null) {
            	actualizar(nodo.right, proveedor);
            }
        }else if(proveedor.getCodigo() == nodo.proveedor.getCodigo()){
        	nodo.proveedor = proveedor;
        }
	}
	//m�todo que llama la funci�n recursiva eliminar sin el nodo
	public void eliminar(Proveedor proveedor){
		eliminar(raiz,proveedor);
	}
	//metodo interno de la clase para utilizarse recursivamente en el arbol
	private void eliminar(Nodo nodo, Proveedor proveedor){
		if ( proveedor.getCodigo() < nodo.proveedor.getCodigo() ) {
            if (nodo.left != null) {
            	eliminar(nodo.left, proveedor);
            }
        } else if ( proveedor.getCodigo() > nodo.proveedor.getCodigo() ) {
            if (nodo.right != null) {
            	eliminar(nodo.right, proveedor);
            }
        }else if(proveedor.getCodigo() == nodo.proveedor.getCodigo()){
        	nodo.proveedor = null;
        	if(raiz.proveedor == proveedor){
        		raiz=null;
        	}
        }
	}
	//m�todo que llama la funci�n recursiva obtenerPorCodigo sin nodo
	public Proveedor obtenerPorCodigo(int codigo) {
		return obtenerPorCodigo(raiz,codigo);
	}
	//metodo interno de la clase para utilizarse recursivamente en el arbol

	private Proveedor obtenerPorCodigo(Nodo raiz, int codigo) {
		if(raiz.proveedor.getCodigo() < codigo){
			return obtenerPorCodigo(raiz.left, codigo);
		}else if(raiz.proveedor.getCodigo() > codigo){
			return obtenerPorCodigo(raiz.right, codigo);
		}else if(raiz.proveedor.getCodigo() == codigo){
			return raiz.proveedor;
		}else{
			return null;
		}
	}
	//metodo para obtener el nodo de la raiz
	public Nodo getRaiz() {
		return raiz;
	}
	//metodo para obtener el numero de elemntos en el arbol
	public int getTama�o(){
		return tama�o;
	}
	
}
