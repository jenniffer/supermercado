package org.upiloto.controlador.excel;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.upiloto.controlador.estructuras.ArbolProductos;
import org.upiloto.controlador.estructuras.ArbolProductos.Nodo;
import org.upiloto.modelo.Producto;

public class GeneradorExcel {
	
    private static final String[] titles = {
            "Codigo", "Nombre", "Marca", "Cantidad", "Precio" ,"Precio total" , "Nombre proveedor"};
	private Sheet sheet;
	private int i;
	private double sumatoria;
    
    public void generarConProductos(ArbolProductos arbolProductos) throws ParseException, IOException {
        
    	//objeto basico para el archivo XLS
    	Workbook wb = new XSSFWorkbook();
    	//Crear libro
        sheet = wb.createSheet("Proveedores");

        //Crear la linea de cabecera para los titulos
        Row headerRow = sheet.createRow(0);
        //define el alto 
        headerRow.setHeightInPoints(12.75f);
        //Crear celdas para cada elemento en el arreglo de titulos
        for (int i = 0; i < titles.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(titles[i]);
        }
        
        i=0;
        sumatoria = 0;
        crearCelda(arbolProductos.getRaiz());
        
        Row resultadoRow = sheet.createRow(i+1);
        resultadoRow.setHeightInPoints(12.75f);
        Cell cell = resultadoRow.createCell(6);
        cell.setCellValue(sumatoria+"");
        
        
        // Escribir el objeto a un archivo 
        String file = "archivo.xlsx";
        FileOutputStream out = new FileOutputStream(file);
        wb.write(out);
        out.close();
    }

	private void crearCelda(Nodo nodoProducto) {
		
		if(nodoProducto.left!=null){
			crearCelda(nodoProducto.left);
		}
        
		//para cada producto crea una fila, para cada propiedad del proveedor crear una celda
    	//fila para el proveedor i, se crea en la fila i+1
    	Row row = sheet.createRow(i+1);
    	row.setHeightInPoints(12.75f);
    	
    	//celda para el codigo del producto i
    	Cell cell_codigo = row.createCell(0);
        cell_codigo.setCellValue(nodoProducto.producto.getCodigo()+"");
        
        //celda para el nombre del producto i
        Cell cell_nombre = row.createCell(1);
        cell_nombre.setCellValue(nodoProducto.producto.getNombre());
        
      //celda para la marca del producto i
        Cell cell_marca = row.createCell(2);
        cell_marca.setCellValue(nodoProducto.producto.getMarca());
        
        String nombreProveedor = nodoProducto.producto.getProveedor().getNombre();
        if(nombreProveedor!=null){
        	Cell cell_proveedor = row.createCell(3);
            cell_proveedor.setCellValue(nombreProveedor);
        }
        
      //celda para la cantidad del producto i
        Cell cell_cantidad = row.createCell(4);
        cell_cantidad.setCellValue(nodoProducto.producto.getCantidad());
        
      //celda para el precio del producto i
        Cell cell_precio = row.createCell(5);
        cell_precio.setCellValue(nodoProducto.producto.getPrecio());
        
        //celda para la sumatoria de los precios de los productos
        Cell cell_precio_total = row.createCell(6);
        cell_precio_total.setCellValue(nodoProducto.producto.getPrecio()*nodoProducto.producto.getCantidad());
        
        sumatoria+=nodoProducto.producto.getPrecio()*nodoProducto.producto.getCantidad();
        
        //aumentar contador
        i++;
        
        if(nodoProducto.right!=null){
			crearCelda(nodoProducto.right);
		}
	}
	
}
