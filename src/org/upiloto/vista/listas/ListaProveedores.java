package org.upiloto.vista.listas;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.upiloto.controlador.Controlador;
import org.upiloto.controlador.estructuras.ArbolProveedores;
import org.upiloto.controlador.estructuras.ArbolProveedores.Nodo;
import org.upiloto.modelo.Proveedor;
import org.upiloto.vista.ContenedorVistas;

public class ListaProveedores extends JPanel implements ActionListener{
	//controlador
	private Controlador controlador;
	//contenedor de vistas
	private ContenedorVistas contenedorVistas;
	
	JPanel jp = new JPanel();
	JScrollPane jsp = new JScrollPane(jp);
	private int i;
	
	public ListaProveedores(){
		setLayout(new GridBagLayout());
		
		controlador = Controlador.getInstance();
		ArbolProveedores arbolProveedores = controlador.getArbolProveedor();
		
		GridBagLayout gbl2 = new GridBagLayout();
		gbl2.columnWeights = new double[] { 0.05f, 0.33f, 0.33f, 0.33f };
		
		jp.removeAll();
		jp.setLayout(gbl2);
		jp.setVisible(true);
		jp.repaint();
		
		GridBagConstraints constraints = new GridBagConstraints();
		
		i=0;
		if(arbolProveedores.getTama�o()>0)
			insertarItems(arbolProveedores.getRaiz());
		
		jsp.setVisible(true);
		jsp.repaint();
		jsp.setPreferredSize(new Dimension(600, 300));
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.gridwidth = 2;
		constraints.gridheight = 1;
		constraints.weighty = 0.2;
		add(jsp, constraints);
	}

	private void insertarItems(final Nodo nodo) {
		
		if(nodo.left!=null){
			insertarItems(nodo.left);
		}
		
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.gridx = 0;
		constraints.gridy = i;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		JLabel label = new JLabel(nodo.proveedor.getNombre()+"/"+nodo.proveedor.getNit());
		jp.add(label, constraints);

		constraints.gridx = 1;
		constraints.gridy = i;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		JButton editarButton = new JButton("Editar");
		jp.add(editarButton, constraints);

		constraints.gridx = 2;
		constraints.gridy = i;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		JButton eliminarButton = new JButton("Eliminar");
		jp.add(eliminarButton, constraints);
		
		constraints.gridx = 3;
		constraints.gridy = i;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		JButton pagoParcialButton = new JButton("R.P. Parcial");
		jp.add(pagoParcialButton, constraints);
		
		constraints.gridx = 4;
		constraints.gridy = i;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		JButton pagoTotalButton = new JButton("R.P. Total");
		jp.add(pagoTotalButton, constraints);
		
		constraints.gridx = 5;
		constraints.gridy = i;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		JButton reporteButton = new JButton("Reporte Pagos");
		jp.add(reporteButton, constraints);
		
		editarButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				contenedorVistas = ContenedorVistas.getInstance();
				contenedorVistas.colocarPanelEdicionProveedor(nodo.proveedor);
			}
		});
		
		eliminarButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int valorMensaje = JOptionPane.showConfirmDialog(null, "Esta seguro que desea eliminar el proveedor ("+nodo.proveedor.getNombre()+"):");
				if(valorMensaje == JOptionPane.OK_OPTION){
					controlador = Controlador.getInstance();
					contenedorVistas = ContenedorVistas.getInstance();
					controlador.eliminarProveedor(nodo.proveedor);
					contenedorVistas.vaciarPanel();
					JOptionPane.showMessageDialog(null, "Proveedor eliminado correctamente.");
				}
			}
		});
		
		pagoParcialButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				contenedorVistas = ContenedorVistas.getInstance();
				contenedorVistas.colocarPanelRegistroPagoParcial(nodo.proveedor);
			}
		});
		
		pagoTotalButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				contenedorVistas = ContenedorVistas.getInstance();
				contenedorVistas.colocarPanelRegistroPagoTotal(nodo.proveedor);
			}
		});
		
		reporteButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				contenedorVistas = ContenedorVistas.getInstance();
				contenedorVistas.colocarPanelListadoPagos(nodo.proveedor);
			}
		});
		i++;
		
		if(nodo.right!=null){
			insertarItems(nodo.right);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		//actualizar capas
		contenedorVistas = ContenedorVistas.getInstance();
		controlador = Controlador.getInstance();
	}
}
