package org.upiloto.vista.listas;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.upiloto.controlador.Controlador;
import org.upiloto.controlador.estructuras.ArbolPagos;
import org.upiloto.controlador.estructuras.ArbolPagos.Nodo;
import org.upiloto.modelo.Pago;
import org.upiloto.modelo.Producto;
import org.upiloto.modelo.Proveedor;
import org.upiloto.vista.ContenedorVistas;

public class ListaPagos extends JPanel implements ActionListener{

	//controlador
	private Controlador controlador;
	//contenedor de vistas
	private ContenedorVistas contenedorVistas;
	private JPanel jp = new JPanel();
	private JScrollPane jsp = new JScrollPane(jp);
	private int i=0;
	
	public ListaPagos(Proveedor proveedor){
		setLayout(new GridBagLayout());
		
		controlador = Controlador.getInstance();
		ArbolPagos arbolPagos = controlador.getArbolPagos();
		
		GridBagLayout gbl2 = new GridBagLayout();
		gbl2.columnWeights = new double[] { 0.05f, 0.33f, 0.33f, 0.33f };
		
		
		jp.removeAll();
		jp.setLayout(gbl2);
		jp.setVisible(true);
		jp.repaint();
		
		insertarItems(arbolPagos);

		jsp.setVisible(true);
		jsp.repaint();
		jsp.setPreferredSize(new Dimension(600, 300));
		
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.gridwidth = 2;
		constraints.gridheight = 1;
		constraints.weighty = 0.2;
		add(jsp, constraints);
	}

	private void insertarItems(ArbolPagos arbolPagos) {
		insertarItems(arbolPagos.getRaiz());
	}

	private void insertarItems(ArbolPagos.Nodo pagoNodo) {
		if(pagoNodo.left!=null){
			insertarItems(pagoNodo.left);
		}
		
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.gridx = 0;
		constraints.gridy = i;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		JLabel label = new JLabel("Referencia: "+ pagoNodo.pago.getReferencia()+" Valor: "+pagoNodo.pago.getValor()+"Saldo:"+pagoNodo.pago.getSaldo());
		jp.add(label, constraints);
		i++;
		
		if(pagoNodo.right!=null){
			insertarItems(pagoNodo.right);
		}
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		//actualizar capas
		contenedorVistas = ContenedorVistas.getInstance();
		controlador = Controlador.getInstance();
		
	}
}

