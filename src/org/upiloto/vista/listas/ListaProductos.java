package org.upiloto.vista.listas;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.upiloto.controlador.Controlador;
import org.upiloto.controlador.estructuras.ArbolProductos;
import org.upiloto.controlador.estructuras.ArbolProductos.Nodo;
import org.upiloto.modelo.Producto;
import org.upiloto.modelo.Proveedor;
import org.upiloto.vista.ContenedorVistas;

public class ListaProductos extends JPanel implements ActionListener{
	//controlador
	private Controlador controlador;
	//contenedor de vistas
	private ContenedorVistas contenedorVistas;
	
	private JPanel jp = new JPanel();
	private JScrollPane jsp = new JScrollPane(jp);
	private int i = 0;
	
	public ListaProductos(){
		setLayout(new GridBagLayout());
		
		controlador = Controlador.getInstance();
		ArbolProductos arbolProductos = controlador.getArbolProductos();
		
		GridBagLayout gbl2 = new GridBagLayout();
		gbl2.columnWeights = new double[] { 0.05f, 0.33f, 0.33f, 0.33f };
		
		jp.removeAll();
		jp.setLayout(gbl2);
		jp.setVisible(true);
		jp.repaint();
		
		GridBagConstraints constraints = new GridBagConstraints();
		
		if(arbolProductos.getTama�o()>0)
			insertarItems(arbolProductos);
		
		jsp.setVisible(true);
		jsp.repaint();
		jsp.setPreferredSize(new Dimension(600, 300));
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.gridwidth = 2;
		constraints.gridheight = 1;
		constraints.weighty = 0.2;
		add(jsp, constraints);
	}

	private void insertarItems(ArbolProductos arbolProductos) {
		insertarItems(arbolProductos.getRaiz());
	}

	private void insertarItems(final ArbolProductos.Nodo nodoProducto) {
		if(nodoProducto.left!=null){
			insertarItems(nodoProducto.left);
		}
		
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.gridx = 0;
		constraints.gridy = i;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		JLabel label = new JLabel(nodoProducto.producto.getNombre());
		jp.add(label, constraints);

		constraints.gridx = 1;
		constraints.gridy = i;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		JButton editarButton = new JButton("Editar");
		jp.add(editarButton, constraints);

		constraints.gridx = 2;
		constraints.gridy = i;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		JButton  borrarButton = new JButton("Eliminar");
		jp.add(borrarButton, constraints);
		
		editarButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				contenedorVistas = ContenedorVistas.getInstance();
				contenedorVistas.colocarPanelEdicionProducto(nodoProducto.producto);
			}
		});
		
		borrarButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int valorMensaje = JOptionPane.showConfirmDialog(null, "Esta seguro que desea eliminar el producto ("+nodoProducto.producto.getNombre()+"):");
				if(valorMensaje == JOptionPane.OK_OPTION){
					controlador = Controlador.getInstance();
					contenedorVistas = ContenedorVistas.getInstance();
					controlador.eliminarProducto(nodoProducto.producto);
					contenedorVistas.vaciarPanel();
					JOptionPane.showMessageDialog(null, "Producto eliminado correctamente.");
				}
			}
		});
		i++;
		
		if(nodoProducto.right!=null){
			insertarItems(nodoProducto.right);
		}
		
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		//actualizar capas
		contenedorVistas = ContenedorVistas.getInstance();
		controlador = Controlador.getInstance();
	}
}
