package org.upiloto.vista;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import org.upiloto.controlador.Controlador;
import org.upiloto.modelo.Producto;
import org.upiloto.modelo.Proveedor;
import org.upiloto.vista.formularios.AgregarProductoFormulario;
import org.upiloto.vista.formularios.AgregarProveedorFormulario;
import org.upiloto.vista.formularios.EliminarProductoFormulario;
import org.upiloto.vista.formularios.EliminarProveedorFormulario;
import org.upiloto.vista.formularios.RegistroPagoParcialFormulario;
import org.upiloto.vista.formularios.RegistroPagoTotalFormulario;
import org.upiloto.vista.listas.ListaPagos;
import org.upiloto.vista.listas.ListaProductos;
import org.upiloto.vista.listas.ListaProveedores;

/**
 * @author Jenniffer Anzola
 *			Ivan Rodriguez
 */

public class ContenedorVistas extends JFrame {

	//tipos de paneles a insertar 
	public enum Paneles {
		AGREGAR_PROVEEDOR,
		ELIMINAR_PROVEEDOR,
		LISTAR_PROVEEDOR,
		AGREGAR_PRODUCTO,
		ELIMINAR_PRODUCTO, 
		LISTAR_PRODUCTO, 
		REGISTRO_PAGO_PARCIAL, 
		REGISTRO_PAGO_TOTAL
	}
	//instancia del controlador
	private Controlador controlador;
	// componentes basicos
	private JPanel panelContenido = new JPanel();
	// ++++++++ c�digo singleton +++++++++++++++
	private static ContenedorVistas contenedorVistas = null;

	/**
	 *Constructor privado que define el t�tulo, el tama�o y
	 *las c�racteristicas b�sicas de la ventana 
	 */
	private ContenedorVistas() {
		super("Supermercado");
		this.setLayout(new GridBagLayout());
		controlador = Controlador.getInstance();
		iniciarVista();
		setSize(800, 600);
		setResizable(false);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
	}

	
	/**
	 * M�todo para obtener la �nica instancia de
	 * la vista principal 
	 * @return instancia del contenedor de paneles
	 */
	public static ContenedorVistas getInstance() {
		if (contenedorVistas == null) {
			contenedorVistas = new ContenedorVistas();
		}
		return contenedorVistas;
	}
	// ++++++++ fin c�digo singleton +++++++++++++++

	
	/**
	 * Este metodo permite acomodar los elementos b�sicos
	 * del contendor principal
	 */
	private void iniciarVista() {
		
		GridBagConstraints gbc = new GridBagConstraints();
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.weightx = 0.5;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		VistaLogo vistaLogo = new VistaLogo();
		add(vistaLogo, gbc);

		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.weightx = 0.5;
		gbc.weighty = 0.05;
		gbc.anchor = GridBagConstraints.NORTHWEST;
		gbc.fill = GridBagConstraints.BOTH;
		VistaMenu vistaMenu = new VistaMenu();
		add(vistaMenu, gbc);

		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.weightx = 0.5;
		gbc.weighty = 1.0;
		gbc.anchor = GridBagConstraints.NORTHWEST;
		gbc.fill = GridBagConstraints.BOTH;
		add(panelContenido, gbc);
	}

	public void vaciarPanel() {
		panelContenido.removeAll();
		panelContenido.setVisible(true);
		panelContenido.repaint();
	}

	public void colocarPanel(Paneles tipo) {
		vaciarPanel();
		this.setVisible(true);
		JPanel panel = null;

		switch (tipo) {
		case AGREGAR_PROVEEDOR:
			panel = new AgregarProveedorFormulario();
			break;
		case ELIMINAR_PROVEEDOR:
			panel = new EliminarProveedorFormulario();
			break;
		case LISTAR_PROVEEDOR:
			panel = new ListaProveedores();
			break;
		case AGREGAR_PRODUCTO:
			panel = new AgregarProductoFormulario();
			break;
		case ELIMINAR_PRODUCTO:
			panel = new EliminarProductoFormulario();
			break;
		case LISTAR_PRODUCTO:
			panel = new ListaProductos();
			break;
		default:
			break;
		}
		panelContenido.add(panel);
		this.setVisible(true);
	}
//M�todo Para colocar el panel de edicion del los datos del proveedor
	public void colocarPanelEdicionProveedor(Proveedor proveedor){
		vaciarPanel();
		this.setVisible(true);
		JPanel panel = new AgregarProveedorFormulario(proveedor);
		panelContenido.add(panel);
		this.setVisible(true);
	}

	//M�todo Para colocar el panel de edicion del los datos del producto

	public void colocarPanelEdicionProducto(Producto producto) {
		vaciarPanel();
		this.setVisible(true);
		JPanel panel = new AgregarProductoFormulario(producto);
		panelContenido.add(panel);
		this.setVisible(true);
	}
	
//M�todo Para colocar el panel de registro del pago parcial

	public void colocarPanelRegistroPagoParcial(Proveedor proveedor) {
		vaciarPanel();
		this.setVisible(true);
		JPanel panel = new RegistroPagoParcialFormulario(proveedor);
		panelContenido.add(panel);
		this.setVisible(true);
	}

	//M�todo Para colocar el panel de registro del pago total

	public void colocarPanelRegistroPagoTotal(Proveedor proveedor) {
		vaciarPanel();
		this.setVisible(true);
		JPanel panel = new RegistroPagoTotalFormulario(proveedor);
		panelContenido.add(panel);
		this.setVisible(true);
	}

	//M�todo Para colocar el panel de la lista de pagos

	public void colocarPanelListadoPagos(Proveedor proveedor) {
		vaciarPanel();
		this.setVisible(true);
		JPanel panel = new ListaPagos(proveedor);
		panelContenido.add(panel);
		this.setVisible(true);
	}
}