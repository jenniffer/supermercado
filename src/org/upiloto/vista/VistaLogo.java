package org.upiloto.vista;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
// clase para traer el logo
public class VistaLogo extends JPanel{
	
	public VistaLogo(){
		File imagen =new File("resources/logo.png");
		JLabel labelImage = null;
		try {
			labelImage= new JLabel(new ImageIcon(ImageIO.read(imagen)));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		add(labelImage);
	}
}
