package org.upiloto.vista;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import org.upiloto.controlador.Controlador;
import org.upiloto.vista.ContenedorVistas.Paneles;

/**
 * @author Jenniffer
 *
 */

public class VistaMenu extends JMenuBar implements ActionListener{
	
	//componentes de la barra de menu
	private JMenu menuArchivo=new JMenu("Archivo");
	private JMenu menuAyuda=new JMenu("Ayuda");
	//elementos internos de cada menu
	private JMenuItem nuevoProveedor=new JMenuItem("Nuevo Proveedor...");
	private JMenuItem listarProveedor=new JMenuItem("Listar Proveedores...");
	private JMenuItem eliminarProveedor=new JMenuItem("Eliminar Proveedor...");
	private JMenuItem nuevoProducto=new JMenuItem("Nuevo Producto...");
	private JMenuItem listarProducto=new JMenuItem("Listar Productos...");
	private JMenuItem eliminarProducto=new JMenuItem("Eliminar Producto...");
	private JMenuItem exportarProducto=new JMenuItem("exportar products a XLS...");
	private JMenuItem cerrar=new JMenuItem("Cerrar...");
	private JMenuItem acercaDe=new JMenuItem("Acerca de...");
	
	//vista principal
	private ContenedorVistas contenedorVistas;
	//controlador
	private Controlador controlador;
	
	/**
	 *Crea la barra de men� con todos los items requeridos. 
	 */
	public VistaMenu(){		
		//agregar elementos al menu de archivo
		menuArchivo.add(nuevoProveedor);
		menuArchivo.add(listarProveedor);
		menuArchivo.add(eliminarProveedor);
		menuArchivo.add(nuevoProducto);
		menuArchivo.add(listarProducto);
		menuArchivo.add(eliminarProducto);
		menuArchivo.add(exportarProducto);
		menuArchivo.add(cerrar);
		//agregar elementos al menu de ayuda
		menuAyuda.add(acercaDe);
		
		//agregar menus a la barra de menus
		add(menuArchivo);
		add(menuAyuda);
		
		nuevoProveedor.addActionListener(this);
		listarProveedor.addActionListener(this);
		eliminarProveedor.addActionListener(this);
		nuevoProducto.addActionListener(this);
		listarProducto.addActionListener(this);
		eliminarProducto.addActionListener(this);
		exportarProducto.addActionListener(this);
		cerrar.addActionListener(this);
		acercaDe.addActionListener(this);
		
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		contenedorVistas=ContenedorVistas.getInstance();
		if(e.getSource()==cerrar){
			System.exit(0);
		}
		if(e.getSource()==acercaDe){
			JOptionPane.showMessageDialog(this, "Dise�ado y desarrollado por Jenniffer Anzola e Ivan Rodriguez. (japtheb@gmail.com, ivanrero@hotmail.com).");
		}
		if(e.getSource()==exportarProducto){
			controlador = Controlador.getInstance();
			controlador.generarExcelProductos();
			JOptionPane.showMessageDialog(this, "Productos exportados.");
		}
		//Asignacion del tipo de panel
		Paneles tipo = null;
		if(e.getSource()==nuevoProveedor){
			tipo = Paneles.AGREGAR_PROVEEDOR;
		}
		if(e.getSource()==listarProveedor){
			tipo = Paneles.LISTAR_PROVEEDOR;
		}
		if(e.getSource()==eliminarProveedor){
			tipo = Paneles.ELIMINAR_PROVEEDOR;
		}
		if(e.getSource()==nuevoProducto){
			tipo = Paneles.AGREGAR_PRODUCTO;
		}
		if(e.getSource()==listarProducto){
			tipo = Paneles.LISTAR_PRODUCTO;
		}
		if(e.getSource()==eliminarProducto){
			tipo = Paneles.ELIMINAR_PRODUCTO;
		}
		//colocar el tipo de panel en el contendor
		if(tipo!=null)
			contenedorVistas.colocarPanel(tipo);
	}
	
}
