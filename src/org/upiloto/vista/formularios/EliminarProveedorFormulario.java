package org.upiloto.vista.formularios;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.upiloto.controlador.Controlador;
import org.upiloto.modelo.Proveedor;
import org.upiloto.vista.ContenedorVistas;

public class EliminarProveedorFormulario extends JPanel implements ActionListener{
	//componentes
	private final JLabel codigoProveedorLabel=new JLabel("C�digo de proveedor");
	
	private final JTextField codigoProveedorTextField=new JTextField("",20);
	
	private final JButton eliminarButton=new JButton("Guardar");
	private final JButton cancelarButton=new JButton("Cancelar");

	//controlador
	private Controlador controlador;
	//contenedor de vistas
	private ContenedorVistas contenedorVistas;
	
	public EliminarProveedorFormulario(){
		setLayout(new GridBagLayout());
		
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.gridx = 0; 
		constraints.gridy = 0; 
		constraints.gridwidth = 1; 
		constraints.gridheight = 1;
		constraints.fill=GridBagConstraints.HORIZONTAL;
		add(codigoProveedorLabel,constraints);
		
		constraints.gridx = 1; 
		constraints.gridy = 0; 
		constraints.gridwidth = 1; 
		constraints.gridheight = 1;
		constraints.fill=GridBagConstraints.HORIZONTAL;
		add(codigoProveedorTextField,constraints);
		
		constraints.gridx = 0; 
		constraints.gridy = 1; 
		constraints.gridwidth = 1; 
		constraints.gridheight = 1;
		constraints.fill=GridBagConstraints.HORIZONTAL;
		add(eliminarButton,constraints);
		
		constraints.gridx = 1; 
		constraints.gridy = 1; 
		constraints.gridwidth = 1; 
		constraints.gridheight = 1;
		constraints.fill=GridBagConstraints.HORIZONTAL;
		add(cancelarButton,constraints);
		
		eliminarButton.addActionListener(this);
		cancelarButton.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		//actualizar capas
		contenedorVistas = ContenedorVistas.getInstance();
		controlador = Controlador.getInstance();
		if(e.getSource()==eliminarButton){
			if(formularioValido()){
				try{
					int codigo = Integer.parseInt( codigoProveedorTextField.getText() );
					Proveedor proveedor = controlador.obtenerProveedorPorCodigo(codigo);
					int valorMensaje = JOptionPane.showConfirmDialog(null, "Esta seguro que desea eliminar el proveedor ("+proveedor.getNombre()+"):");
					if(valorMensaje == JOptionPane.OK_OPTION){
						controlador.eliminarProveedor(proveedor);
						contenedorVistas.vaciarPanel();
						JOptionPane.showMessageDialog(null, "Proveedor eliminado correctamente.");
					}
				}catch(Exception error){
					JOptionPane.showMessageDialog(null, "El c�digo debe ser un n�mero entero.");
				}
				
			}else{
				JOptionPane.showMessageDialog(null, "Ingrese el codigo del preveedor.");
			}
		}
		if(e.getSource()==cancelarButton){
			contenedorVistas.vaciarPanel();
		}
	}

	private boolean formularioValido() {
		if(codigoProveedorTextField.getText().equals(""))
			return false;
		return true;
	}
}
