package org.upiloto.vista.formularios;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.upiloto.controlador.Controlador;
import org.upiloto.modelo.Producto;
import org.upiloto.modelo.Proveedor;
import org.upiloto.vista.ContenedorVistas;

public class AgregarProductoFormulario extends JPanel implements ActionListener{
	//componentes
	private final JLabel nombreProductoLabel=new JLabel("Nombre de producto");
	private final JLabel marcaProductoLabel=new JLabel("Marca de producto");
	private final JLabel codigoProductoLabel=new JLabel("C�digo de producto");
	private final JLabel cantidadProductoLabel=new JLabel("Cantidad de producto");
	private final JLabel precioProductoLabel=new JLabel("Precio de producto");
	private final JLabel proveedorProductoLabel=new JLabel("Proveedor de producto");
	
	private final JTextField nombreProductoTextField=new JTextField("",20);
	private final JTextField marcaProductoTextField=new JTextField("",20);
	private final JTextField codigoProductoTextField=new JTextField("",20);
	private final JTextField cantidadProductoTextField=new JTextField("",20);
	private final JTextField precioProductoTextField=new JTextField("",20);
	private JComboBox proveedorProductoCombo = new JComboBox();
	
	private final JButton guardarButton=new JButton("Guardar");
	private final JButton cancelarButton=new JButton("Cancelar");

	//controlador
	private Controlador controlador;
	//contenedor de vistas
	private ContenedorVistas contenedorVistas;
	private Proveedor proveedorSeleccionado;
	
	public AgregarProductoFormulario(){
		iniciarPanel();
	}
	
	public AgregarProductoFormulario(Producto producto) {
		iniciarPanel();
		nombreProductoTextField.setText(producto.getNombre());
		marcaProductoTextField.setText(producto.getMarca());
		codigoProductoTextField.setText(producto.getCodigo()+"");
		cantidadProductoTextField.setText(producto.getCantidad()+"");
		precioProductoTextField.setText(producto.getPrecio()+"");
	}

	private void iniciarPanel() {
		setLayout(new GridBagLayout());
		
		controlador = Controlador.getInstance();
		final Proveedor[] proveedores = controlador.obtenerTodosProveedores();
		if(proveedores.length>0){
			proveedorProductoCombo = new JComboBox(controlador.obtenerTodosProveedoresString());
			proveedorSeleccionado = proveedores[0];
			proveedorProductoCombo.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					JComboBox cb = (JComboBox)e.getSource();
					int seleccion = cb.getSelectedIndex();
					proveedorSeleccionado = proveedores[seleccion];
				}
			});
			
			GridBagConstraints constraints = new GridBagConstraints();
			constraints.gridx = 0; 
			constraints.gridy = 0; 
			constraints.gridwidth = 1; 
			constraints.gridheight = 1;
			constraints.fill=GridBagConstraints.HORIZONTAL;
			add(nombreProductoLabel,constraints);
			
			constraints.gridx = 1; 
			constraints.gridy = 0; 
			constraints.gridwidth = 1; 
			constraints.gridheight = 1;
			constraints.fill=GridBagConstraints.HORIZONTAL;
			add(nombreProductoTextField,constraints);
			
			constraints.gridx = 0; 
			constraints.gridy = 1; 
			constraints.gridwidth = 1; 
			constraints.gridheight = 1;
			constraints.fill=GridBagConstraints.HORIZONTAL;
			add(marcaProductoLabel,constraints);
			
			constraints.gridx = 1; 
			constraints.gridy = 1; 
			constraints.gridwidth = 1; 
			constraints.gridheight = 1;
			constraints.fill=GridBagConstraints.HORIZONTAL;
			add(marcaProductoTextField,constraints);
			
			constraints.gridx = 0; 
			constraints.gridy = 2; 
			constraints.gridwidth = 1; 
			constraints.gridheight = 1;
			constraints.fill=GridBagConstraints.HORIZONTAL;
			add(codigoProductoLabel,constraints);
			
			constraints.gridx = 1; 
			constraints.gridy = 2; 
			constraints.gridwidth = 1; 
			constraints.gridheight = 1;
			constraints.fill=GridBagConstraints.HORIZONTAL;
			add(codigoProductoTextField,constraints);
			
			constraints.gridx = 0; 
			constraints.gridy = 3; 
			constraints.gridwidth = 1; 
			constraints.gridheight = 1;
			constraints.fill=GridBagConstraints.HORIZONTAL;
			add(cantidadProductoLabel,constraints);
			
			constraints.gridx = 1; 
			constraints.gridy = 3; 
			constraints.gridwidth = 1; 
			constraints.gridheight = 1;
			constraints.fill=GridBagConstraints.HORIZONTAL;
			add(cantidadProductoTextField,constraints);
			
			constraints.gridx = 0; 
			constraints.gridy = 4; 
			constraints.gridwidth = 1; 
			constraints.gridheight = 1;
			constraints.fill=GridBagConstraints.HORIZONTAL;
			add(precioProductoLabel,constraints);
			
			constraints.gridx = 1; 
			constraints.gridy = 4; 
			constraints.gridwidth = 1; 
			constraints.gridheight = 1;
			constraints.fill=GridBagConstraints.HORIZONTAL;
			add(precioProductoTextField,constraints);
			
			constraints.gridx = 0; 
			constraints.gridy = 5; 
			constraints.gridwidth = 1; 
			constraints.gridheight = 1;
			constraints.fill=GridBagConstraints.HORIZONTAL;
			add(proveedorProductoLabel,constraints);
			
			constraints.gridx = 1; 
			constraints.gridy = 5; 
			constraints.gridwidth = 1; 
			constraints.gridheight = 1;
			constraints.fill=GridBagConstraints.HORIZONTAL;
			add(proveedorProductoCombo,constraints);
			
			constraints.gridx = 0; 
			constraints.gridy = 8; 
			constraints.gridwidth = 1; 
			constraints.gridheight = 1;
			constraints.fill=GridBagConstraints.HORIZONTAL;
			add(guardarButton,constraints);
			
			constraints.gridx = 1; 
			constraints.gridy = 8; 
			constraints.gridwidth = 1; 
			constraints.gridheight = 1;
			constraints.fill=GridBagConstraints.HORIZONTAL;
			add(cancelarButton,constraints);
			
			guardarButton.addActionListener(this);
			cancelarButton.addActionListener(this);
		}else{
			JOptionPane.showMessageDialog(null, "No hay Proveedores.");
		}
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		//actualizar capas
		contenedorVistas = ContenedorVistas.getInstance();
		controlador = Controlador.getInstance();
		if(e.getSource()==guardarButton){
			Producto producto = formularioValido();
			if(producto!=null){
				controlador.guardarProducto(producto);
				contenedorVistas.vaciarPanel();
				JOptionPane.showMessageDialog(null, "Producto guardado correctamente.");
			}else{
				JOptionPane.showMessageDialog(null, "Revise el formulario.");
			}
		}
		if(e.getSource()==cancelarButton){
			contenedorVistas.vaciarPanel();
		}
	}

	private Producto formularioValido() {
		if(nombreProductoTextField.getText().equals(""))
			return null;
		if(marcaProductoTextField.getText().equals(""))
			return null;
		if(codigoProductoTextField.getText().equals(""))
			return null;
		if(cantidadProductoTextField.getText().equals(""))
			return null;
		if(precioProductoTextField.getText().equals(""))
			return null;
		try{
			double precio = Double.parseDouble(precioProductoTextField.getText() );
			int cantidad = Integer.parseInt(cantidadProductoTextField.getText() );
			int codigo = Integer.parseInt(codigoProductoTextField.getText() );
			return new Producto(nombreProductoTextField.getText(), 
					marcaProductoTextField.getText(), 
					codigo, 
					cantidad, 
					precio,
					proveedorSeleccionado
					);
		}catch(Exception e){
			return null;
		}
	}
}
