package org.upiloto.vista.formularios;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.upiloto.controlador.Controlador;
import org.upiloto.modelo.Producto;
import org.upiloto.modelo.Proveedor;
import org.upiloto.vista.ContenedorVistas;

public class AgregarProveedorFormulario extends JPanel implements ActionListener{
	//componentes
	private final JLabel nombreProveedorLabel=new JLabel("Nombre de proveedor");
	private final JLabel codigoProveedorLabel=new JLabel("C�digo de proveedor");
	private final JLabel ciudadProveedorLabel=new JLabel("Ciudad de proveedor");
	private final JLabel telefonoProveedorLabel=new JLabel("Tel�fono de proveedor");
	private final JLabel emailProveedorLabel=new JLabel("Email de proveedor");
	private final JLabel direccionProveedorLabel=new JLabel("Direcci�n de proveedor");
	private final JLabel cuentaProveedorLabel=new JLabel("Cuenta del proveedor");
	private final JLabel nitProveedorLabel=new JLabel("NIT de proveedor");
	
	private final JTextField nombreProveedorTextField=new JTextField("",20);
	private final JTextField codigoProveedorTextField=new JTextField("",20);
	private final JTextField ciudadProveedorTextField=new JTextField("",20);
	private final JTextField telefonoProveedorTextField=new JTextField("",20);
	private final JTextField emailProveedorTextField=new JTextField("",20);
	private final JTextField direccionProveedorTextField=new JTextField("",20);
	private final JTextField cuentaProveedorTextField=new JTextField("",20);
	private final JTextField nitProveedorTextField=new JTextField("",20);
	
	private final JButton guardarButton=new JButton("Guardar");
	private final JButton cancelarButton=new JButton("Cancelar");

	//controlador
	private Controlador controlador;
	//contenedor de vistas
	private ContenedorVistas contenedorVistas;
	
	public AgregarProveedorFormulario(){
		iniciarPanel();
	}
	
	public AgregarProveedorFormulario(Proveedor proveedor) {
		iniciarPanel();
		nombreProveedorTextField.setText(proveedor.getNombre());
		codigoProveedorTextField.setText(proveedor.getCodigo()+"");
		ciudadProveedorTextField.setText(proveedor.getCiudad());
		telefonoProveedorTextField.setText(proveedor.getTelefono());
		emailProveedorTextField.setText(proveedor.getEmail());
		direccionProveedorTextField.setText(proveedor.getDireccion());
		cuentaProveedorTextField.setText(proveedor.getCuenta());
		nitProveedorTextField.setText(proveedor.getNit());
	}

	private void iniciarPanel() {
		setLayout(new GridBagLayout());
		
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.gridx = 0; 
		constraints.gridy = 0; 
		constraints.gridwidth = 1; 
		constraints.gridheight = 1;
		constraints.fill=GridBagConstraints.HORIZONTAL;
		add(nombreProveedorLabel,constraints);
		
		constraints.gridx = 1; 
		constraints.gridy = 0; 
		constraints.gridwidth = 1; 
		constraints.gridheight = 1;
		constraints.fill=GridBagConstraints.HORIZONTAL;
		add(nombreProveedorTextField,constraints);
		
		constraints.gridx = 0; 
		constraints.gridy = 1; 
		constraints.gridwidth = 1; 
		constraints.gridheight = 1;
		constraints.fill=GridBagConstraints.HORIZONTAL;
		add(codigoProveedorLabel,constraints);
		
		constraints.gridx = 1; 
		constraints.gridy = 1; 
		constraints.gridwidth = 1; 
		constraints.gridheight = 1;
		constraints.fill=GridBagConstraints.HORIZONTAL;
		add(codigoProveedorTextField,constraints);
		
		constraints.gridx = 0; 
		constraints.gridy = 2; 
		constraints.gridwidth = 1; 
		constraints.gridheight = 1;
		constraints.fill=GridBagConstraints.HORIZONTAL;
		add(ciudadProveedorLabel,constraints);
		
		constraints.gridx = 1; 
		constraints.gridy = 2; 
		constraints.gridwidth = 1; 
		constraints.gridheight = 1;
		constraints.fill=GridBagConstraints.HORIZONTAL;
		add(ciudadProveedorTextField,constraints);
		
		constraints.gridx = 0; 
		constraints.gridy = 3; 
		constraints.gridwidth = 1; 
		constraints.gridheight = 1;
		constraints.fill=GridBagConstraints.HORIZONTAL;
		add(telefonoProveedorLabel,constraints);
		
		constraints.gridx = 1; 
		constraints.gridy = 3; 
		constraints.gridwidth = 1; 
		constraints.gridheight = 1;
		constraints.fill=GridBagConstraints.HORIZONTAL;
		add(telefonoProveedorTextField,constraints);
		
		constraints.gridx = 0; 
		constraints.gridy = 4; 
		constraints.gridwidth = 1; 
		constraints.gridheight = 1;
		constraints.fill=GridBagConstraints.HORIZONTAL;
		add(direccionProveedorLabel,constraints);
		
		constraints.gridx = 1; 
		constraints.gridy = 4; 
		constraints.gridwidth = 1; 
		constraints.gridheight = 1;
		constraints.fill=GridBagConstraints.HORIZONTAL;
		add(direccionProveedorTextField,constraints);
		
		constraints.gridx = 0; 
		constraints.gridy = 5; 
		constraints.gridwidth = 1; 
		constraints.gridheight = 1;
		constraints.fill=GridBagConstraints.HORIZONTAL;
		add(emailProveedorLabel,constraints);
		
		constraints.gridx = 1; 
		constraints.gridy = 5; 
		constraints.gridwidth = 1; 
		constraints.gridheight = 1;
		constraints.fill=GridBagConstraints.HORIZONTAL;
		add(emailProveedorTextField,constraints);
	
		constraints.gridx = 0; 
		constraints.gridy = 6; 
		constraints.gridwidth = 1; 
		constraints.gridheight = 1;
		constraints.fill=GridBagConstraints.HORIZONTAL;
		add(cuentaProveedorLabel,constraints);
		
		constraints.gridx = 1; 
		constraints.gridy = 6; 
		constraints.gridwidth = 1; 
		constraints.gridheight = 1;
		constraints.fill=GridBagConstraints.HORIZONTAL;
		add(cuentaProveedorTextField,constraints);
		
		constraints.gridx = 0; 
		constraints.gridy = 7; 
		constraints.gridwidth = 1; 
		constraints.gridheight = 1;
		constraints.fill=GridBagConstraints.HORIZONTAL;
		add(nitProveedorLabel,constraints);
		
		constraints.gridx = 1; 
		constraints.gridy = 7; 
		constraints.gridwidth = 1; 
		constraints.gridheight = 1;
		constraints.fill=GridBagConstraints.HORIZONTAL;
		add(nitProveedorTextField,constraints);
		
		constraints.gridx = 0; 
		constraints.gridy = 8; 
		constraints.gridwidth = 1; 
		constraints.gridheight = 1;
		constraints.fill=GridBagConstraints.HORIZONTAL;
		add(guardarButton,constraints);
		
		constraints.gridx = 1; 
		constraints.gridy = 8; 
		constraints.gridwidth = 1; 
		constraints.gridheight = 1;
		constraints.fill=GridBagConstraints.HORIZONTAL;
		add(cancelarButton,constraints);
		
		guardarButton.addActionListener(this);
		cancelarButton.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		//actualizar capas
		contenedorVistas = ContenedorVistas.getInstance();
		controlador = Controlador.getInstance();
		if(e.getSource()==guardarButton){
			Proveedor proveedor = formularioValido();
			if(proveedor!=null){
				controlador.guardarProveedor(proveedor);
				contenedorVistas.vaciarPanel();
				JOptionPane.showMessageDialog(null, "Proveedor guardado correctamente.");
			}else{
				JOptionPane.showMessageDialog(null, "Revise el formulario.");
			}
		}
		if(e.getSource()==cancelarButton){
			contenedorVistas.vaciarPanel();
		}
	}

	private Proveedor formularioValido() {
		if(nombreProveedorTextField.getText().equals(""))
			return null;
		if(telefonoProveedorTextField.getText().equals(""))
			return null;
		if(emailProveedorTextField.getText().equals(""))
			return null;
		if(direccionProveedorTextField.getText().equals(""))
			return null;
		if(nitProveedorTextField.getText().equals(""))
			return null;
		if(codigoProveedorTextField.getText().equals(""))
			return null;
		if(ciudadProveedorTextField.getText().equals(""))
			return null;
		if(cuentaProveedorTextField.getText().equals(""))
			return null;
		try{
			int codigo = Integer.parseInt(codigoProveedorTextField.getText() );
			return new Proveedor(nombreProveedorTextField.getText(),
								codigo,
								ciudadProveedorTextField.getText(),
								telefonoProveedorTextField.getText(),
								emailProveedorTextField.getText(),
								direccionProveedorTextField.getText(),
								cuentaProveedorTextField.getText(),
								nitProveedorTextField.getText()
								);
		}catch(Exception e){
			return null;
		}
	}
}
