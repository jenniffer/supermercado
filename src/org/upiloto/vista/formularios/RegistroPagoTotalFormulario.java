package org.upiloto.vista.formularios;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.upiloto.controlador.Controlador;
import org.upiloto.modelo.Pago;
import org.upiloto.modelo.Producto;
import org.upiloto.modelo.Proveedor;
import org.upiloto.vista.ContenedorVistas;

public class RegistroPagoTotalFormulario extends JPanel implements ActionListener{
	//componentes
	private final JLabel referenciaPagoLabel=new JLabel("Referencia");
	private final JLabel valortotalPagoLabel=new JLabel("Valor del pago");
	
	private final JTextField referenciaPagoTextField=new JTextField("",20);
	private final JTextField valortotalPagoTextField=new JTextField("",20);
	
	private final JButton guardarButton=new JButton("Guardar");
	private final JButton cancelarButton=new JButton("Cancelar");

	//controlador
	private Controlador controlador;
	//contenedor de vistas
	private ContenedorVistas contenedorVistas;
	//proveedor
	private Proveedor proveedor;
	
	public RegistroPagoTotalFormulario(Proveedor proveedor){
		iniciarPanel();
		this.proveedor = proveedor;
	}

	private void iniciarPanel() {
		setLayout(new GridBagLayout());
		
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.gridx = 0; 
		constraints.gridy = 0; 
		constraints.gridwidth = 1; 
		constraints.gridheight = 1;
		constraints.fill=GridBagConstraints.HORIZONTAL;
		add(referenciaPagoLabel,constraints);
		
		constraints.gridx = 1; 
		constraints.gridy = 0; 
		constraints.gridwidth = 1; 
		constraints.gridheight = 1;
		constraints.fill=GridBagConstraints.HORIZONTAL;
		add(referenciaPagoTextField,constraints);
		
		constraints.gridx = 0; 
		constraints.gridy = 1; 
		constraints.gridwidth = 1; 
		constraints.gridheight = 1;
		constraints.fill=GridBagConstraints.HORIZONTAL;
		add(valortotalPagoLabel,constraints);
		
		constraints.gridx = 1; 
		constraints.gridy = 1; 
		constraints.gridwidth = 1; 
		constraints.gridheight = 1;
		constraints.fill=GridBagConstraints.HORIZONTAL;
		add(valortotalPagoTextField,constraints);
			
		constraints.gridx = 0; 
		constraints.gridy = 2; 
		constraints.gridwidth = 1; 
		constraints.gridheight = 1;
		constraints.fill=GridBagConstraints.HORIZONTAL;
		add(guardarButton,constraints);
		
		constraints.gridx = 1; 
		constraints.gridy = 2; 
		constraints.gridwidth = 1; 
		constraints.gridheight = 1;
		constraints.fill=GridBagConstraints.HORIZONTAL;
		add(cancelarButton,constraints);
		
		guardarButton.addActionListener(this);
		cancelarButton.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		//actualizar capas
		contenedorVistas = ContenedorVistas.getInstance();
		controlador = Controlador.getInstance();
		if(e.getSource()==guardarButton){
			Pago pago = formularioValido();
			if(pago!=null){
				controlador.guardarPago(pago);
				contenedorVistas.vaciarPanel();
				JOptionPane.showMessageDialog(null, "Pago guardado correctamente.");
			}else{
				JOptionPane.showMessageDialog(null, "Revise el formulario.");
			}
		}
		if(e.getSource()==cancelarButton){
			contenedorVistas.vaciarPanel();
		}
	}

	private Pago formularioValido() {
		if(valortotalPagoTextField.getText().equals(""))
			return null;
		if(referenciaPagoTextField.getText().equals(""))
			return null;
		try{
			double valor = Double.parseDouble(valortotalPagoTextField.getText() );
			int referencia = Integer.parseInt(referenciaPagoTextField.getText());
			return new Pago(valor,0,referencia,this.proveedor);
		}catch(Exception e){
			return null;
		}
	}
}
