package org.upiloto.vista.formularios;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.upiloto.controlador.Controlador;
import org.upiloto.modelo.Producto;
import org.upiloto.vista.ContenedorVistas;

public class EliminarProductoFormulario extends JPanel implements ActionListener{
	//componentes
	private final JLabel codigoProductoLabel=new JLabel("C�digo de producto");
	
	private final JTextField codigoProductoTextField=new JTextField("",20);
	
	private final JButton eliminarButton=new JButton("Guardar");
	private final JButton cancelarButton=new JButton("Cancelar");

	//controlador
	private Controlador controlador;
	//contenedor de vistas
	private ContenedorVistas contenedorVistas;
	
	public EliminarProductoFormulario(){
		setLayout(new GridBagLayout());
		
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.gridx = 0; 
		constraints.gridy = 0; 
		constraints.gridwidth = 1; 
		constraints.gridheight = 1;
		constraints.fill=GridBagConstraints.HORIZONTAL;
		add(codigoProductoLabel,constraints);
		
		constraints.gridx = 1; 
		constraints.gridy = 0; 
		constraints.gridwidth = 1; 
		constraints.gridheight = 1;
		constraints.fill=GridBagConstraints.HORIZONTAL;
		add(codigoProductoTextField,constraints);
		
		constraints.gridx = 0; 
		constraints.gridy = 1; 
		constraints.gridwidth = 1; 
		constraints.gridheight = 1;
		constraints.fill=GridBagConstraints.HORIZONTAL;
		add(eliminarButton,constraints);
		
		constraints.gridx = 1; 
		constraints.gridy = 1; 
		constraints.gridwidth = 1; 
		constraints.gridheight = 1;
		constraints.fill=GridBagConstraints.HORIZONTAL;
		add(cancelarButton,constraints);
		
		eliminarButton.addActionListener(this);
		cancelarButton.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		//actualizar capas
		contenedorVistas = ContenedorVistas.getInstance();
		controlador = Controlador.getInstance();
		if(e.getSource()==eliminarButton){
			if(formularioValido()){
				try{
					int codigo = Integer.parseInt(codigoProductoTextField.getText());
					Producto producto = controlador.obtenerProductoPorCodigo(codigo);
					int valorMensaje = JOptionPane.showConfirmDialog(null, "Esta seguro que desea eliminar el proveedor ("+producto.getNombre()+"):");
					if(valorMensaje == JOptionPane.OK_OPTION){
						controlador.eliminarProducto(producto);
						contenedorVistas.vaciarPanel();
						JOptionPane.showMessageDialog(null, "Proveedor eliminado correctamente.");
					}
				}catch(Exception error){
					JOptionPane.showMessageDialog(null, "El codigo debe ser un numero.");
				}
			}else{
				JOptionPane.showMessageDialog(null, "Ingrese el codigo del preveedor.");
			}
		}
		if(e.getSource()==cancelarButton){
			contenedorVistas.vaciarPanel();
		}
	}

	private boolean formularioValido() {
		if(codigoProductoTextField.getText().equals(""))
			return false;
		return true;
	}
}
