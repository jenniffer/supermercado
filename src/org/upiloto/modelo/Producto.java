package org.upiloto.modelo;

//clase producto
public class Producto {
	//Atributos
	
	private String nombre;
	private String marca;
	private int codigo;
	private int cantidad;
	private double precio;
	private Proveedor proveedor;
	
	//constructor de la clase
	public Producto(String nombre, String marca, int codigo, int cantidad, double precio, Proveedor proveedor) {
		super();
		this.nombre = nombre;
		this.marca = marca;
		this.codigo = codigo;
		this.cantidad = cantidad;
		this.precio = precio;
		this.proveedor = proveedor;
	}
	//getters y setters de la clase
	public Proveedor getProveedor() {
		return proveedor;
	}
	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	
}
