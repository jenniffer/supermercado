package org.upiloto.modelo;

//clase pago
public class Pago {
	//Atributos de la clase
	private double valor;
	private double saldo;
	private int referencia;
	private Proveedor proveedor;
	
	// constructor de la clase
	public Pago(double valor, double saldo, int referencia, Proveedor proveedor) {
		super();
		this.valor = valor;
		this.saldo = saldo;
		this.referencia = referencia;
		this.proveedor = proveedor;
	}
	//getters y setters 
	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public int getReferencia() {
		return referencia;
	}

	public void setReferencia(int referencia) {
		this.referencia = referencia;
	}

	public Proveedor getProveedor() {
		return proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}
	
}
