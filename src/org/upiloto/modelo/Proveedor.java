package org.upiloto.modelo;

//Clase proveedor
public class Proveedor {
	
	private String nombre;
	private int codigo;
	private String ciudad;
	private String telefono;
	private String email;
	private String direccion;
	private String cuenta;
	private String nit;
	
	//constructor de la clase
	public Proveedor(String nombre, int codigo, String ciudad, String telefono, String email, String direccion, String cuenta, String nit) {
		super();
		this.nombre = nombre;
		this.codigo = codigo;
		this.ciudad = ciudad;
		this.telefono = telefono;
		this.email = email;
		this.direccion = direccion;
		this.cuenta = cuenta;
		this.nit = nit;
	}
	//getters y setters de la clase
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getNit() {
		return nit;
	}
	public void setNit(String nit) {
		this.nit = nit;
	}
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public String getCuenta() {
		return cuenta;
	}
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	
}
